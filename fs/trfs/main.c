/*
 * Copyright (c) 1998-2015 Erez Zadok
 * Copyright (c) 2009	   Shrikar Archak
 * Copyright (c) 2003-2015 Stony Brook University
 * Copyright (c) 2003-2015 The Research Foundation of SUNY
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */

#include "trfs.h"
#include "trctl.h"
#include <linux/module.h>
#include <linux/parser.h>
#include <linux/file.h>
#include <linux/slab.h>

/* Required Data structures*/
struct trfs_mount_opts {
	char *tfile;
};

struct trfs_super_data {
	char *data;
	struct file *tfile;
};

enum {
	trfs_opt_tfile
};
static const match_table_t tokens = {
	{trfs_opt_tfile, "tfile=%s"},
};

static int trfs_parse_options(char *data, struct trfs_mount_opts *opts){
	substring_t args[MAX_OPT_ARGS];
	//int option;
	int token;
	char *p;
	char *fname;

	while ((p = strsep(&data, ",")) != NULL) {
		if (!*p)
			continue;
		token = match_token(p, tokens, args);
		switch (token) {
		case trfs_opt_tfile:
			fname = match_strdup(&args[0]);
			if (!fname) {
				printk(KERN_ERR "Not enough memory for storing trace-file name");
				return -1;
			}
			opts->tfile = fname;
			break;
		default:
			return -EINVAL;
		}
	}

	return 0;
}

/*
 * There is no need to lock the trfs_super_info's rwsem as there is no
 * way anyone can have a reference to the superblock at this point in time.
 */
static int trfs_read_super(struct super_block *sb, void *raw_data, int silent)
{
	int err = 0;
	struct super_block *lower_sb;
	struct path lower_path;
	char *dev_name = ((struct trfs_super_data *)raw_data)->data;
	struct inode *inode;
	struct file *tfile;

	if (!dev_name) {
		printk(KERN_ERR
		       "trfs: read_super: missing dev_name argument\n");
		err = -EINVAL;
		goto out;
	}

	/* parse lower path */
	err = kern_path(dev_name, LOOKUP_FOLLOW | LOOKUP_DIRECTORY,
			&lower_path);
	if (err) {
		printk(KERN_ERR	"trfs: error accessing "
		       "lower directory '%s'\n", dev_name);
		goto out;
	}

	/* allocate superblock private data */
	sb->s_fs_info = kzalloc(sizeof(struct trfs_sb_info), GFP_KERNEL);
	if (!trfs_SB(sb)) {
		printk(KERN_CRIT "trfs: read_super: out of memory\n");
		err = -ENOMEM;
		goto out_free;
	}

	/* set the lower superblock field of upper superblock */
	lower_sb = lower_path.dentry->d_sb;
	atomic_inc(&lower_sb->s_active);
	trfs_set_lower_super(sb, lower_sb);

	/* set tfile in superblock private*/
	tfile = ((struct trfs_super_data *)raw_data)->tfile;
	trfs_set_filp_super(sb,tfile);

	/* set the record id to start from 0*/
	trfs_set_record_count(sb);

	/* set the op_tr_flag*/
	trfs_set_op_flags_super(sb,TRFS_ALL);

	/* allocate memory to write buffer , remember to free when un-mounting */
	trfs_SB(sb)->record_buff = kmalloc(PAGE_SIZE,GFP_KERNEL);
	if(trfs_SB(sb)->record_buff == NULL){
		printk(KERN_DEBUG "Error creating buffer\n");
		err = -ENOMEM;
		goto out_sput;
	}

	/* set buffer pointer to 0*/
	atomic_set(&trfs_SB(sb)->buff_pos,0);

	/* inherit maxbytes from lower file system */
	sb->s_maxbytes = lower_sb->s_maxbytes;

	/*
	 * Our c/m/atime granularity is 1 ns because we may stack on file
	 * systems whose granularity is as good.
	 */
	sb->s_time_gran = 1;

	sb->s_op = &trfs_sops;

	sb->s_export_op = &trfs_export_ops; /* adding NFS support */

	/* get a new inode and allocate our root dentry */
	inode = trfs_iget(sb, d_inode(lower_path.dentry));
	if (IS_ERR(inode)) {
		err = PTR_ERR(inode);
		goto out_sput;
	}
	sb->s_root = d_make_root(inode);
	if (!sb->s_root) {
		err = -ENOMEM;
		goto out_iput;
	}
	d_set_d_op(sb->s_root, &trfs_dops);

	/* link the upper and lower dentries */
	sb->s_root->d_fsdata = NULL;
	err = new_dentry_private_data(sb->s_root);
	if (err)
		goto out_freeroot;

	/* if get here: cannot have error */

	/* set the lower dentries for s_root */
	trfs_set_lower_path(sb->s_root, &lower_path);

	/*
	 * No need to call interpose because we already have a positive
	 * dentry, which was instantiated by d_make_root.  Just need to
	 * d_rehash it.
	 */
	d_rehash(sb->s_root);
	if (!silent)
		printk(KERN_INFO
		       "trfs: mounted on top of %s type %s\n",
		       dev_name, lower_sb->s_type->name);
	goto out; /* all is well */

	/* no longer needed: free_dentry_private_data(sb->s_root); */
out_freeroot:
	dput(sb->s_root);
out_iput:
	iput(inode);
out_sput:
	/* drop refs we took earlier */
	if(trfs_SB(sb)->record_buff != NULL){
		kfree(trfs_SB(sb)->record_buff);
	}
	if(trfs_get_filp_super(sb) != NULL){
		filp_close(trfs_get_filp_super(sb), NULL);
	}
	atomic_dec(&lower_sb->s_active);
	kfree(trfs_SB(sb));
	sb->s_fs_info = NULL;
out_free:
	path_put(&lower_path);

out:
	return err;
}

static struct dentry *trfs_mount(struct file_system_type *fs_type, int flags,
		const char *dev_name, void *raw_data) {
	int rc;
	struct trfs_mount_opts *opts;
	struct file *tfile = NULL;
	struct trfs_super_data trfs_data;

	trfs_data.data = (char *)dev_name;
	opts = kmalloc(sizeof(struct trfs_mount_opts), GFP_KERNEL);
	if (!opts) {
		rc = -ENOMEM;
		goto out;
	}

	rc = trfs_parse_options(raw_data, opts);
	if (rc) {
		printk(KERN_ERR "Error parsing options");
		goto out;
	}
	printk(KERN_INFO "-tfile:%s",opts->tfile);

	tfile = filp_open(opts->tfile, O_TRUNC | O_CREAT | O_WRONLY, S_IWUSR);
	if (!tfile || IS_ERR(tfile)) {
		printk(KERN_ERR "Error reading/writing Trace file\n");
		rc = PTR_ERR(tfile);
		goto out;
	}

	/* Check if its a file and not a directory or something other*/
	if (!S_ISREG(file_inode(tfile)->i_mode)) {
		printk(KERN_ERR "trfs: tfile not a file\n");
		rc = -ENOENT;
		goto out;
	}
	trfs_data.tfile = tfile;

	/*create workqueue*/
	rc = trfs_init_workqueue();
	if(rc)
		goto out;

	/*clean options structure*/
	kfree(opts->tfile);
	kfree(opts);
	return mount_nodev(fs_type, flags, (void *) &trfs_data, trfs_read_super);
out:
	if (opts != NULL && opts->tfile != NULL)
		kfree(opts->tfile);
	if (opts != NULL)
		kfree(opts);
	if (tfile != NULL)
		filp_close(tfile, NULL);
	return ERR_PTR(rc);
}

static struct file_system_type trfs_fs_type = {
	.owner		= THIS_MODULE,
	.name		= trfs_NAME,
	.mount		= trfs_mount,
	.kill_sb	= generic_shutdown_super,
	.fs_flags	= 0,
};
MODULE_ALIAS_FS(trfs_NAME);

static int __init init_trfs_fs(void)
{
	int err;

	pr_info("Registering trfs " TRFS_VERSION "\n");

	err = trfs_init_inode_cache();
	if (err)
		goto out;
	err = trfs_init_dentry_cache();
	if (err)
		goto out;
	err = register_filesystem(&trfs_fs_type);
out:
	if (err) {
		trfs_destroy_inode_cache();
		trfs_destroy_dentry_cache();
	}
	return err;
}

static void __exit exit_trfs_fs(void)
{
	trfs_destroy_inode_cache();
	trfs_destroy_dentry_cache();
	unregister_filesystem(&trfs_fs_type);
	pr_info("Completed trfs module unload\n");
}

MODULE_AUTHOR("Erez Zadok, Filesystems and Storage Lab, Stony Brook University"
	      " (http://www.fsl.cs.sunysb.edu/)");
MODULE_DESCRIPTION("trfs " TRFS_VERSION
		   " (http://trfs.filesystems.org/)");
MODULE_LICENSE("GPL");

module_init(init_trfs_fs);
module_exit(exit_trfs_fs);
