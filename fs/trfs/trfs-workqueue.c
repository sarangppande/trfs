#include "trfs.h"
#include "trctl.h"
/*Work queue*/
struct workqueue_struct *trfs_workqueue = 0;

/*
 * trfs_init_workqueue - Initializes workqueue
 *
 * This function return 0 on success, -1 on failure
*/
int trfs_init_workqueue(void)
{
	if(!trfs_workqueue){
		trfs_workqueue = create_singlethread_workqueue("trfsWorkqueue");
		if(trfs_workqueue == NULL){
			return -1;
		}
		printk(KERN_INFO "Work-queue Ready.....");
	}
	return 0;
}

/*
 * trfs_init_workqueue - Destroys workqueue
 *
*/
void trfs_destroy_workqueue(void){
	if(trfs_workqueue){
		printk(KERN_INFO "Destroying workqueue...");
		destroy_workqueue(trfs_workqueue);
	}
}

/*
 * thread_readwrite - Writes record into the tfile
 * @work_arg : pointer to workqueue work struct
 *
*/
static void thread_readwrite(struct work_struct *work_arg) {
	struct trfs_work *data;
	struct trfs_record record;
	int ret;
	int buffer_pos=0;
	char *readwrite_buff;
	struct file *read_file;
	mm_segment_t prev_fs;

	data = container_of(work_arg,struct trfs_work,work);

	/*assign record id*/
	record.recordID = trfs_get_record_count(data->sb);
	record.record_size = strlen(data->record_data->path) +
						sizeof(struct trfs_record_data);


	/*
	printk(KERN_INFO "Record type: %d",data->record_data->record_type);
	printk(KERN_INFO "Record Flag: %d",data->record_data->flags);
	printk(KERN_INFO "Record Mode: %d",data->record_data->mode);
	printk(KERN_INFO "Record Seek: %d",data->record_data->seek);
	printk(KERN_INFO "Record Path Length: %d",data->record_data->path_length);
	printk(KERN_INFO "Record Buff len: %ld",data->record_data->buff_len);
	printk(KERN_INFO "Record Path: %s",data->record_data->path);
	*/

	/*Copy record  into buffer*/
	memcpy(trfs_SB(data->sb)->record_buff,&record,sizeof(struct trfs_record));
	buffer_pos = sizeof(struct trfs_record);
	/*Copy data into buffer*/
	memcpy(trfs_SB(data->sb)->record_buff+buffer_pos,
			data->record_data,record.record_size);
	buffer_pos += record.record_size;


	/*Check if it is not a read or write function and filename is still
	 * set that means it is dentry function*/
	if(data->record_data->record_type & (TRFS_LINK | TRFS_SYMLINK)){
		memcpy(trfs_SB(data->sb)->record_buff+buffer_pos,
				data->filepath,strlen(data->filepath));
		buffer_pos += strlen(data->filepath);
	}

	/*Write record to tFile*/
	prev_fs = get_fs();
	set_fs(get_ds());
	ret = vfs_write(trfs_SB(data->sb)->tfile, trfs_SB(data->sb)->record_buff,
			buffer_pos,&(trfs_SB(data->sb)->tfile)->f_pos);
	if(ret > 0){
		printk(KERN_INFO "Record written");
	}else{
		printk(KERN_ERR "error: %d",ret);
		printk(KERN_INFO "Problem Writing to log file");
	}
	set_fs(prev_fs);


	/*if write operation then copy from lower file and write to tFile*/
	if(data->record_data->ret > 0 && data->count > 0){
		readwrite_buff = kmalloc(data->count,GFP_KERNEL);
		read_file = filp_open(data->filepath,O_RDONLY, 0);
		if(!read_file || IS_ERR(read_file)) {
			ret = PTR_ERR(read_file);
			printk(KERN_ERR "Error opening file:%d\n", ret);
		}
		prev_fs = get_fs();
		set_fs(get_ds());
		ret = vfs_read(trfs_lower_file(read_file),
				readwrite_buff,data->count,&data->pos);
		if(ret > 0){
			ret = vfs_write(data->tfile, readwrite_buff,ret,
						&(data->tfile)->f_pos);
			if( ret > 0){
				printk(KERN_INFO "file content written");
			}else{
				printk(KERN_ERR "error: %d",ret);
				printk(KERN_INFO "Problem Writing to log file");
			}
		}else{
			printk(KERN_ERR "error: %d",ret);
			printk(KERN_INFO "Problem Reading File");
		}
		set_fs(prev_fs);
		filp_close(read_file, NULL);
		kfree(readwrite_buff);
	}

	//More Clean Up
	if(data->filepath != NULL){
		/*if record type was SYMLINK then filepath is different*/
		if(data->record_data->record_type & TRFS_SYMLINK){
			kfree(data->filepath);
			printk(KERN_INFO "Yes I have been tested !!!");
		}
		/*
		 * Releasing it here because dentry_path_raw and d_path stores
		 * the path at an offset in the buffer,so to free filpath we
		 * need the starting address of buffer and hence the need for
		 * tmp_buffer
		*/
		kfree(data->tmp_buffer);
	}
	if(data->record_data != NULL)
		kfree(data->record_data);
	if(data != NULL)
		kfree(data);
}

/*
 * set_trfs_record - Creates record structure
 * @size : size of struct
 * @type : type of record
 * @flags: File Flags
 * @mode : File mode
 * @ssize: Count size
 * @seek : seek position
 * @path_length : length of path
 * @path : path
 * @ret  : Return Value
 * This function return record of success , NULL on failure
*/
struct trfs_record_data *set_trfs_record(int size, unsigned short type,
		unsigned int flags, umode_t mode, int ssize, int seek,
		int path_length, const char *path, int ret)
{
	struct trfs_record_data *rec = kmalloc(size, GFP_KERNEL);
	if(rec == NULL){
		return NULL;
	}
	rec->record_type = type;
	rec->flags = flags;
	rec->mode = mode;
	rec->ssize = ssize;
	rec->seek = seek;
	rec->ret = ret;
	rec->path_length = path_length;
	strncpy(rec->path,path, path_length);
	strncpy(rec->path+path_length,"\0", 1);
	return rec;
}

/*
 * trfs_record_file_action - Records file actions
 * @rtype : type of record
 * @file: File descriptor
 * @pos : seek position
 * @count: Count size
 * @err  : Return Value
 * This function return 0 of success , -1 on failure
*/
int trfs_record_file_action(unsigned short rtype,struct file *file, loff_t pos,
		size_t count,int err)
{
	int path_len;
	struct trfs_work *t;
	char *tmp_buffer,*path;

	if(trfs_workqueue){
		tmp_buffer = kmalloc(PAGE_SIZE,GFP_KERNEL);
		if(tmp_buffer == NULL ){
			/*we won't throw any error , because we want the FS to work*/
			printk(KERN_ERR "Error Allocating Memory");
			goto free;
		}

		path = dentry_path_raw(file->f_path.dentry, tmp_buffer, PAGE_SIZE);
		path_len = strlen(path);

		t = kmalloc(sizeof(struct trfs_work), 	GFP_KERNEL);
		if(t == NULL ){
			/*we won't throw any error , because we want the FS to work*/
			printk(KERN_ERR "Error Allocating Memory");
			goto free_tmp_buffer;
		}
		t->record_data =
			set_trfs_record(path_len + sizeof(struct trfs_record_data)+1,
					rtype,file->f_flags,file->f_mode, count,pos,path_len,
					path, err);
		if(t->record_data == NULL){
			printk(KERN_ERR "Error Creating object");
			goto free_t;
		}

		t->sb = file_inode(file)->i_sb;
		t->pos = 0;
		t->count = 0;
		t->record_data->buff_len = 0;
		t->tfile = trfs_get_filp_super(file_inode(file)->i_sb);
		t->filepath = NULL;
		if(err > 0 && count > 0 && (rtype & (TRFS_READ | TRFS_WRITE)) ){
			t->filepath = d_path(&file->f_path, tmp_buffer, PAGE_SIZE);
			if(t->filepath == NULL ){
				/*we won't throw any error , because we want the FS to work*/
				printk(KERN_ERR "Error Allocating Memory");
				goto free_record;
			}
			t->record_data->buff_len = count;
			t->pos = pos;
			t->count = count;
		}
		t->tmp_buffer = tmp_buffer;
		INIT_WORK( &(t->work), thread_readwrite );
		queue_work(trfs_workqueue, &(t->work));
	}

	return 0;

free_record:
	kfree(t->record_data);
free_t:
	kfree(t);
free_tmp_buffer:
	kfree(tmp_buffer);
free:
	return -1;
}

/*
 * trfs_record_dentry_actions - Records dentry actions
 * @rtype : type of record
 * @dentry1: dentry struct
 * @dentry2: dentry struct
 * @mode : mode flag
 * @symlink: Symlink string
 * @err  : Return Value
 * This function return 0 of success , -1 on failure
*/
int trfs_record_dentry_actions(unsigned short rtype,struct dentry *dentry1,
		struct dentry *dentry2,umode_t mode,char *symlink,int err){
	int path_len;
	struct trfs_work *t;
	char *path1;
	char *tmp_buffer;
	if(trfs_workqueue){
		tmp_buffer = kmalloc(PAGE_SIZE, GFP_KERNEL);
		if(tmp_buffer == NULL ){
			/*we won't throw any error , because we want the FS to work*/
			printk(KERN_ERR "Error Allocating Memory");
			goto free;
		}

		t = kmalloc(sizeof(struct trfs_work), GFP_KERNEL);
		if(t == NULL ){
			/*we won't throw any error , because we want the FS to work*/
			printk(KERN_ERR "Error Allocating Memory");
			goto free_tmp_buffer;
		}
		t->sb = dentry1->d_sb;
		t->tfile = trfs_get_filp_super(t->sb);

		path1 = dentry_path_raw(dentry1, tmp_buffer, PAGE_SIZE);
		if (IS_ERR(path1)) {
			printk(KERN_ERR "Error reading file path");
			goto free_t;
		}

		path_len = strlen(path1);
		t->record_data =
				set_trfs_record(path_len + sizeof(struct trfs_record_data)+1,
							rtype,0,mode, 0, 0, path_len,
							path1, err);
		if(t->record_data == NULL){
			printk(KERN_INFO "Error creating record.");
			goto free_t;
		}
		t->count = 0;
		t->record_data->buff_len = 0;
		t->pos = 0;
		t->filepath = NULL;

		if(dentry2 != NULL){
			t->filepath = dentry_path_raw(dentry2, tmp_buffer, PAGE_SIZE);
			if (IS_ERR(t->filepath)) {
				printk(KERN_ERR "Error reading file path");
				goto free_record;
			}
			t->record_data->buff_len = strlen(t->filepath);
		}

		if(symlink != NULL){
			t->record_data->buff_len = strlen(symlink)+1;
			t->filepath = kmalloc(t->record_data->buff_len,GFP_KERNEL);
			if(t->filepath == NULL){
				printk(KERN_ERR "Unable to allocate memory");
				goto free;
			}
			strncpy(t->filepath,symlink,t->record_data->buff_len-1);
			strncpy(t->filepath+t->record_data->buff_len-1,"\0",1);
		}

		t->tmp_buffer = tmp_buffer;
		INIT_WORK( &(t->work), thread_readwrite );
		queue_work(trfs_workqueue, &(t->work));
	}
	return 0;

free_record:
	kfree(t->record_data);
free_t:
	kfree(t);
free_tmp_buffer:
	kfree(tmp_buffer);
free:
	return -1;
}

