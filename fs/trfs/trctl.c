#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/types.h>
#include <unistd.h>
#include <ctype.h>
#include <sys/stat.h>
#include <linux/ioctl.h>
#include "trctl.h"

//#define EXTRA_CREDIT
#define READP	"+read"
#define READM	"-read"
#define WRITEP	"+write"
#define WRITEM	"-write"
#define OPENP	"+open"
#define OPENM	"-open"
#define CREATP	"+create"
#define CREATM	"-create"
#define LINKP	"+link"
#define LINKM	"-link"
#define UNLINKP	"+unlink"
#define UNLINKM	"-unlink"
#define SYLINKP	"+symlink"
#define SYLINKM "-symlink"
#define MKDIRP	"+mkdir"
#define MKDIRM	"-mkdir"
#define RMDIRP	"+rmdir"
#define RMDIRM	"-rmdir"
#define LSEEKP	"+lseek"
#define LSEEKM	"-lseek"
#define ALL		"all"
#define NONE	"none"
#define HEX		"0x"

int read_inc_options(int argc, char* argv[], unsigned int *bitmap) {
	int i;
	for (i = 1; i < argc - 1; i++) {
		if (strcmp(argv[i], READP) == 0) {
			*bitmap |= TRFS_READ;
		} else if (strcmp(argv[i], READM) == 0) {
			*bitmap &= ~TRFS_READ;
		} else if (strcmp(argv[i], WRITEP) == 0) {
			*bitmap |= TRFS_WRITE;
		} else if (strcmp(argv[i], WRITEM) == 0) {
			*bitmap &= ~TRFS_WRITE;
		} else if (strcmp(argv[i], OPENP) == 0) {
			*bitmap |= TRFS_OPEN;
		} else if (strcmp(argv[i], OPENM) == 0) {
			*bitmap &= ~TRFS_OPEN;
		} else if (strcmp(argv[i], CREATP) == 0) {
			*bitmap |= TRFS_CREATE;
		} else if (strcmp(argv[i], CREATM) == 0) {
			*bitmap &= ~TRFS_CREATE;
		} else if (strcmp(argv[i], LINKP) == 0) {
			*bitmap |= TRFS_LINK;
		} else if (strcmp(argv[i], LINKM) == 0) {
			*bitmap &= ~TRFS_LINK;
		} else if (strcmp(argv[i], UNLINKP) == 0) {
			*bitmap |= TRFS_UNLINK;
		} else if (strcmp(argv[i], UNLINKM) == 0) {
			*bitmap &= ~TRFS_UNLINK;
		} else if (strcmp(argv[i], SYLINKP) == 0) {
			*bitmap |= TRFS_SYMLINK;
		} else if (strcmp(argv[i], SYLINKM) == 0) {
			*bitmap &= ~TRFS_SYMLINK;
		} else if (strcmp(argv[i], MKDIRP) == 0) {
			*bitmap |= TRFS_MKDIR;
		} else if (strcmp(argv[i], MKDIRM) == 0) {
			*bitmap &= ~TRFS_MKDIR;
		} else if (strcmp(argv[i], RMDIRP) == 0) {
			*bitmap |= TRFS_RMDIR;
		} else if (strcmp(argv[i], RMDIRM) == 0) {
			*bitmap &= ~TRFS_RMDIR;
		} else if (strcmp(argv[i], LSEEKP) == 0) {
			*bitmap |= TRFS_LSEEK;
		} else if (strcmp(argv[i], LSEEKM) == 0) {
			*bitmap &= ~TRFS_LSEEK;
		} else if (strcmp(argv[i], ALL) == 0) {
			*bitmap |= TRFS_ALL;
		} else if (strcmp(argv[i], NONE) == 0) {
			*bitmap |= TRFS_NONE;
		} else {
			return -1;
		}
	}
	return 0;
}
int main(int argc, char* argv[]) {

	int i, input_fd;
	static char use[] = "%s : usage= ./trctl CMD /mounted/path\n";
	char *all = "all";
	char *none = "none";
	char *cmd;
	char *path;
	unsigned int bitmap;
	int ret_val;
	if (argc < 2) {
		fprintf(stderr, "%s: please provide correct arguments\n", argv[0]);
		fprintf(stderr, use, argv[0]);
		exit(1);
	}
	//bitmap = malloc(sizeof(unsigned int));
	if (argc == 2) {
		path = argv[1];
		input_fd = open(path, O_DIRECTORY);
		if (input_fd < 0) {
			fprintf(stderr, "%s: cant open mount path directory\n",argv[0]);
			exit(0);
		}
		cmd = NULL;
	} else {
#ifdef EXTRA_CREDIT
		path = argv[argc - 1];
		cmd = "extra";
		input_fd = open(path, O_DIRECTORY);
		if (input_fd < 0) {
			fprintf(stderr,"%s: cant open mount path directory\n", argv[0]);
			exit(0);
		}
		/* Retrieve current flag and then perfrom operations on that */
		ret_val = ioctl(input_fd, IOCTL_GET_MSG, &bitmap);
		if (ret_val < 0) {
			close(input_fd);
			fprintf(stderr,"%s: ioctl_get_msg failed:%d\n",argv[0],ret_val);
			exit(-1);
		}
		if ( read_inc_options(argc, argv, &bitmap) < 0) {
			fprintf(stderr, "%s: please provide correct arguments\n", argv[0]);
			fprintf(stderr, use, argv[0]);
			exit(1);
		}
#else
		/*Normal behavior*/
		path = argv[2];
		cmd = argv[1];
		/*storing hex value*/
		if (strcmp(cmd, ALL) == 0)
			bitmap = TRFS_ALL;
		else if (strcmp(cmd, NONE) == 0)
			bitmap = TRFS_NONE;
		else if (strncmp(cmd, HEX,2) == 0 && isxdigit(*cmd) != 0) {
			sscanf(cmd, "%x", &bitmap);
			if(bitmap > TRFS_ALL){
				fprintf(stderr, "%s: Hex value exceeds max limit\n"
						,argv[0]);
				exit(1);
			}
		} else {
			fprintf(stderr, "%s: please provide correct arguments\n", argv[0]);
			fprintf(stderr, use, argv[0]);
			exit(1);
		}
		input_fd = open(path, O_DIRECTORY);
		if (input_fd < 0) {
			fprintf(stderr, "%s: cant open mount path directory\n", argv[0]);
			exit(0);
		}
#endif
	}

	if (cmd == NULL) {
		ret_val = ioctl(input_fd, IOCTL_GET_MSG, &bitmap);
		if (ret_val < 0) {
			close(input_fd);
			fprintf(stderr, "%s: ioctl_get_msg failed:%d\n", argv[0],ret_val);
			exit(-1);
		}
		printf("bitmap set in hex: %x\n", bitmap);
	} else {
		ret_val = ioctl(input_fd, IOCTL_SET_MSG, &bitmap);
		if (ret_val < 0) {
			close(input_fd);
			fprintf(stderr, "%s: ioctl_set_msg failed:%d\n",argv[0], ret_val);
			exit(-1);
		}
	}
	close(input_fd);
	exit(1);
}
