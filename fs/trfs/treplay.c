#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <unistd.h>
#include <linux/limits.h>
#include <linux/types.h>
#include <string.h>
#include <fcntl.h>
#include "trctl.h"

/* flags for -ns options */
#define N_FLAG 0x01
#define S_FLAG 0x02

/* trfs_record struct */
struct trfs_record
{
	unsigned int recordID;
	unsigned short record_size;
};

/* trfs_record_data struct */
struct trfs_record_data
{
	unsigned short record_type;
	unsigned int flags;
	unsigned short mode;
	int ssize;
	int seek;
	unsigned short path_length;
	unsigned long buff_len;
	int ret;
	char path[0];
};

int main(int argc, const char *argv[])
{
	int option;
	/* tfilepath stores trace file name */
	char tfilepath[PATH_MAX];
	int tfile_fp;
	/* buffer to read data when read system call */
	char *read_buff;
	/* buffer to store write data when write system call or
	 * second filename in case of link, symlink calls*/
	char *content;
	int fd, ret;
	struct trfs_record record;
	struct trfs_record_data *data;
	/* Set all flags to off and read them using getopt */
	unsigned char flags = 0;
	long bytes = 0;
	static char usage[] = "usage: ./ %s [-ns] TFILE\n";
	/*Check if less arguments are provided*/
	if (argc < 2)
	{
		fprintf(stderr, "%s: please provide all arguments\n", argv[0]);
		fprintf(stderr, usage, argv[0]);
		exit(1);
	}
	/* set flags when options are set */
	while ((option = getopt(argc, (char * const *) argv, "ns")) != -1)
	{
		switch (option)
		{
		case 'n':
			flags |= N_FLAG;
			break;
		case 's':
			flags |= S_FLAG;
			break;
		}
	}
	/* if both options are set abort */
	if ((flags & 3) == 3)
	{
		fprintf(stderr, "%s: Incorrect usage: n and s can't be together\n",
				argv[0]);
		exit(1);
	}
	/* Copy tfile name*/
	if (flags == 0)
	{
		strcpy(tfilepath, argv[1]);
	}
	else
	{
		strcpy(tfilepath, argv[2]);
	}

	/*Open trace file*/
	tfile_fp = open(tfilepath, O_RDONLY);
	if (!tfile_fp)
	{
		fprintf(stderr, "%s: Unable to open trace file\n", argv[0]);
		exit(1);
	}
	/*read from file*/
	bytes = read(tfile_fp, &record, sizeof(struct trfs_record));
	data = malloc(record.record_size);
	/* read from trace file till there is a record */
	while (bytes > 0)
	{
		read(tfile_fp, data, record.record_size);
		/* if write data is there, store it in content buffer */
		if (data->buff_len > 0)
		{
			content = malloc(data->buff_len);
			read(tfile_fp, content, data->buff_len);
		}

		/*Decide operation here*/

		switch (data->record_type)
		{
		/* Open system call. Print the record and replay it if s flag is
		 * set or no flag is set. If the original descriptor was positive
		 * and the current one is negative or the original descriptor was
		 * negative and current one is positive or if both are negative
		 * but error is different then it is a deviation. */
		case TRFS_OPEN:
			printf("Record : %u\n", record.recordID);
			printf("Record type : OPEN\n");
			printf("Path name : %s\n", data->path + 1);
			if (flags != N_FLAG)
			{
				fd = open(data->path + 1, data->flags);
				if (((fd > 0 && data->ret < 0) || (fd < 0 && data->ret > 0)
						|| (fd < 0 && data->ret < 0 && fd != data->ret))
						&& flags == S_FLAG)
				{
					fprintf(stderr, "%s : Deviation occurred  in open\n",
							argv[0]);
					fprintf(stderr, "%s Prev value : %d, New value = %d\n",
							argv[0], data->ret, fd);
					close(fd);
					goto free;
				}
				close(fd);
			}
			break;

			/* Write system call. Open the file, lseek to set the offset if there
			 * is any and write to the file. If no. of bytes written is different
			 * from the original, it is a deviation */
		case TRFS_WRITE:
			printf("Record : %u\n", record.recordID);
			printf("Record type : WRITE\n");
			printf("Path name : %s\n", data->path + 1);
			if (flags != N_FLAG)
			{
				fd = open(data->path + 1, data->flags);
				lseek(fd, data->seek, SEEK_SET);
				if (data->buff_len > 0)
				{
					ret = write(fd, content, data->buff_len);
					if (ret != data->ret && flags == S_FLAG)
					{
						fprintf(stderr, "%s : Deviation occurred  in write \n",
								argv[0]);
						fprintf(stderr, "%s : Prev value: %d, New value: %d\n",
								argv[0], data->ret, ret);
						close(fd);
						free(content);
						goto free;
					}
					close(fd);
				}
			}
			break;

			/* Read system call. Open the file, lseek to set the offset if there
			 * is any and read from the file. If no. of bytes read is different
			 * from the original, or if the original read buffer is different
			 * from the current read buffer, it is a deviation */
		case TRFS_READ:
			printf("Record : %u\n", record.recordID);
			printf("Record type : READ\n");
			printf("Path name : %s\n", data->path + 1);
			if (flags != N_FLAG)
			{
				fd = open(data->path + 1, data->flags);
				lseek(fd, data->seek, SEEK_SET);
				if (data->buff_len > 0)
				{
					read_buff = malloc(data->ssize);
					ret = read(fd, read_buff, data->ssize);
					if ((strcmp(read_buff, content) != 0 || ret != data->ret)
							&& flags == S_FLAG)
					{
						fprintf(stderr, "%s : Deviation occurred  in read \n",
								argv[0]);
						fprintf(stderr, "%s : Prev value: %d, New value: %d\n",
								argv[0], data->ret, ret);
						fprintf(stderr, "%s : Prev read buffer : %s, "
								"New read buffer : %s\n", argv[0], content,
								read_buff);
						close(fd);
						free(read_buff);
						goto free;
					}
					printf("%s\n", read_buff);
					close(fd);
					free(read_buff);
				}
			}
			break;

			/* Create system call. Create the file in the specified mode.
			 * If the original descriptor was positive and the current one is
			 * negative or the original descriptor was negative and current one
			 * is positive or if both are negative but error is different then
			 * it is a deviation. */
		case TRFS_CREATE:
			printf("Record : %u\n", record.recordID);
			printf("Record type : CREAT\n");
			printf("Path name : %s\n", data->path + 1);
			if (flags != N_FLAG)
			{
				fd = creat(data->path + 1, data->mode);
				if (((fd > 0 && data->ret < 0) || (fd < 0 && data->ret > 0)
						|| (fd < 0 && data->ret < 0 && fd != data->ret))
						&& flags == S_FLAG)
				{
					fprintf(stderr, "%s : Deviation occurred  in create\n",
							argv[0]);
					fprintf(stderr, "%s : Prev value : %d, New value : %d\n",
							argv[0], data->ret, fd);
					goto free;
				}
			}
			break;

			/* Link system call. Here content buffer stores the second filename.*/
		case TRFS_LINK:
			printf("Record : %u\n", record.recordID);
			printf("Record type : LINK\n");
			printf("Path name : %s\n", data->path + 1);
			if (flags != N_FLAG)
			{
				printf("content = %s\n", content);
				fd = link(data->path + 1, content + 1);
				if (fd != data->ret && flags == S_FLAG)
				{
					fprintf(stderr, "%s : Deviation occurred  in link\n",
							argv[0]);
					fprintf(stderr, "%s : Prev value : %d, New value : %d\n",
							argv[0], data->ret, fd);
					goto free;
				}
			}
			break;

			/* Unlink system call. */
		case TRFS_UNLINK:
			printf("Record : %u\n", record.recordID);
			printf("Record type : UNLINK\n");
			printf("Path name : %s\n", data->path + 1);
			if (flags != N_FLAG)
			{
				fd = unlink(data->path + 1);
				if (fd != data->ret && flags == S_FLAG)
				{
					fprintf(stderr, "%s : Deviation occurred  in unlink\n",
							argv[0]);
					fprintf(stderr, "%s : Prev value : %d, New value : %d\n",
							argv[0], data->ret, fd);
					goto free;
				}
			}
			break;

			/* Symlink system call. Here content buffer stores the old filename
			 * and data->path stores the new filename.*/
		case TRFS_SYMLINK:
			printf("Record : %u\n", record.recordID);
			printf("Record type : SYMLINK\n");
			printf("Path name : %s\n", data->path + 1);
			if (flags != N_FLAG)
			{
				printf("content = %s\n", content);
				fd = symlink(content + 1, data->path + 1);
				if (fd != data->ret && flags == S_FLAG)
				{
					fprintf(stderr, "%s : Deviation occurred  in symlink\n",
							argv[0]);
					fprintf(stderr, "%s : Prev value : %d, New value : %d\n",
							argv[0], data->ret, fd);
					goto free;
				}
			}
			break;

			/* Mkdir system call */
		case TRFS_MKDIR:
			printf("Record : %u\n", record.recordID);
			printf("Record type : MKDIR\n");
			printf("Path name : %s\n", data->path + 1);
			if (flags != N_FLAG)
			{
				fd = mkdir(data->path + 1, data->mode);
				if (fd != data->ret && flags == S_FLAG)
				{
					fprintf(stderr, "%s : Deviation occurred  in mkdir\n",
							argv[0]);
					fprintf(stderr, "%s : Prev value : %d, New value : %d\n",
							argv[0], data->ret, fd);
					goto free;
				}
			}
			break;

			/* Rmdir system call */
		case TRFS_RMDIR:
			printf("Record : %u\n", record.recordID);
			printf("Record type : RMDIR\n");
			printf("Path name : %s\n", data->path + 1);
			if (flags != N_FLAG)
			{
				fd = rmdir(data->path + 1);
				if (fd != data->ret && flags == S_FLAG)
				{
					fprintf(stderr, "%s : Deviation occurred  in rmdir\n",
							argv[0]);
					fprintf(stderr, "%s : Prev value : %d, New value : %d\n",
							argv[0], data->ret, fd);
					goto free;
				}
			}
			break;

			/* Lseek system call. data->seek stores the offset and data-ssize
			 * stores the whence argument.*/
		case TRFS_LSEEK:
			printf("Record : %u\n", record.recordID);
			printf("Record type : LSEEK\n");
			printf("Path name : %s\n", data->path + 1);
			if (flags != N_FLAG)
			{
				fd = open(data->path + 1, data->flags);
				ret = lseek(fd, data->seek, data->ssize);
				if (ret != data->ret && flags == S_FLAG)
				{
					fprintf(stderr, "%s : Deviation occurred  in lseek\n",
							argv[0]);
					fprintf(stderr, "%s : Prev value : %d, New value : %d\n",
							argv[0], data->ret, ret);
					goto free;
				}
			}
			break;

		}
		if (data->buff_len > 0)
		{
			free(content);
		}
		/* read next record */
		bytes = read(tfile_fp, &record, sizeof(struct trfs_record));
	}

	/* successful exit */
	close(tfile_fp);
	exit(0);
	/* free label if abort in between */
	free:
	close(tfile_fp);
	free(data);
	exit(1);
}
